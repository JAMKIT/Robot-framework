#!/bin/bash
set +e

#ENV variables
GIT_TEST_REPO=${GIT_TEST_REPO:=}
GIT_REPO_NAME=${GIT_REPO_NAME:=}
TEST_SCRIPT=${TEST_SCRIPT:=}
RESOURCE_FILE=${RESOURCE_FILE:=}
SUT_IP_ADDRESS=${SUT_IP_ADDRESS:=}
TEST_FILES_FOLDER=${TEST_FILES_FOLDER:=}
JENKINS_FOLDER=${JENKINS_FOLDER:=}
DEFAULT_PATH="/home/root/test"

echo 'Starting Xvfb ...'
export DISPLAY=:99
2>/dev/null 1>&2 Xvfb :99 -shmem -screen 0 1920x1200x16 &
exec "$@"

mkdir -p $DEFAULT_PATH
cd $DEFAULT_PATH

echo 'Cloning test repo...'
git clone $GIT_TEST_REPO

#change resource.txt ip address
sed -i "s/IP_ADDRESS/$SUT_IP_ADDRESS/g" $DEFAULT_PATH/$TEST_FILES_FOLDER/$RESOURCE_FILE

echo 'Starting robot tests...'
cd $DEFAULT_PATH/$GIT_REPO_NAME
sh $DEFAULT_PATH/$GIT_REPO_NAME/$TEST_SCRIPT

echo "moving test results to jenkins"
mv $DEFAULT_PATH/$TEST_FILES_FOLDER/log.html $JENKINS_FOLDER/log.html
mv $DEFAULT_PATH/$TEST_FILES_FOLDER/output.xml $JENKINS_FOLDER/output.xml
mv $DEFAULT_PATH/$TEST_FILES_FOLDER/report.html $JENKINS_FOLDER/report.html
